module.exports = {
  mode: "development",
  entry: './main.js',
  output: {
    filename: 'app.compiled.js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: ['babel-loader']
      }
    ]
  },
};
import { registerAction } from './state.lib';

export const countAction = (state) => {
  state.counter++;

  return state;
};

export const count = registerAction(countAction);
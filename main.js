import React from 'react';
import ReactDom from 'react-dom';
import { initState, connectComponent } from './state.lib';
import { count } from './actions';

class Component1 extends React.Component {
  render() {
    return (
      <React.Fragment>
        <p>{ this.props.counter }</p>
        <button onClick={this.props.onClick}>Click me!</button>
      </React.Fragment>
    );
  }
};

const mapState = (state) => ({
  counter: state.counter
});

const mapActions = () => ({
  onClick: count
});

const Component1Connector = connectComponent(mapState, mapActions)(Component1);

initState({ counter: 555 });

ReactDom.render(
  <Component1Connector />, 
  document.getElementById('app')
);
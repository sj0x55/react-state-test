import React from 'react';
import fastCopy from 'fast-copy';
import fastDeepEqual from 'fast-deep-equal';

const stateUpdaters = {};
const state = {
  counter: 0
};

const wrappedComponent = (Component, mapState) => {
  return class extends React.Component {
    constructor(props) {
      super(props);
  
      this.state = {};
      this.stateUpdaterHash = Math.random().toString(36).substring(2);
      this.currentProps = { ...this.props };

      stateUpdaters[this.stateUpdaterHash] = (state) => {
        this.currentProps = { ...this.props, ...mapState(state) };
        
        if (this.shouldComponentUpdate(this.props, this.state)) {
          this.setState(this.state);
        }
      };
    }
  
    shouldComponentUpdate(props, state) {
      return !fastDeepEqual(props, this.currentProps);
    }
  
    componentWillUnmount() {
      delete stateUpdaters[this.stateUpdaterHash];
    }
  
    render() {
      return (<Component {...{
        ...this.props,
        ...this.currentProps
      }} />);
    }
  };
};

export const initState = (newState) => {
  Object.assign(state, newState);
};

export const registerAction = (subscriber, name) => (...args) => {
  const copyState = fastCopy(state);

  Object.assign(state, subscriber(copyState, ...args));
  Object.keys(stateUpdaters).forEach(hash => stateUpdaters[hash](copyState));
};

export const connectComponent = (mapState, mapActions) => (Component) => {
  return (props) => React.createElement(
    wrappedComponent(Component, mapState), 
    {
      ...props,
      ...mapActions(),
      ...mapState(state)
    }
  );
};